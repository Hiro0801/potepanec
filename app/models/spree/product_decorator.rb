module Spree::ProductDecorator
  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct
  end

  def self.prepended(base)
    base.scope :latest_order, -> do
      base.includes(master: [:default_price, :images]).order(available_on: :desc)
    end
  end

  Spree::Product.prepend self
end
