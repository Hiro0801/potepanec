class Potepan::HomePagesController < ApplicationController
  NEW_PRODUCTS_LIMIT = 8
  def index
    @new_products = Spree::Product.latest_order.select(&:available?).first(NEW_PRODUCTS_LIMIT)
  end
end
