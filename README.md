_●OSSであるSolidusを拡張したECサイトでございます。_  

・URL: https://potepanec-20200325130826.herokuapp.com/potepan  



_●使用技術_   

・Mac OS  
・使用言語：HTML, CSS, JavaScript, jQuery, Ruby, SQL  
・フレームワーク  
　　⚪Ruby：Ruby on Rails  
　　⚪Test：RSpec  
　　⚪CSS：Bootstrap  
・DB：MySQL  
・OSS：Solidus  
・リンター：Rubocop  
・インフラ：Docker, AWS S3, Heroku  
・デプロイ：Circle CI  
・バージョン管理：Git (Github Flowを用いた、擬似的なチーム開発を実施)  
・その他のツール：Bitbucket  



_●アプリの機能詳細_  

基本機能  
・トップページ  
・新着商品表示機能  
・人気カテゴリー表示機能  
・カテゴリーページ  
・カテゴリー商品一覧表示機能  
・商品詳細ページ  
・商品詳細表示機能  
・関連商品表示機能  

その他  
・Rspecによる、単体テスト、統合テスト  