require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon_1) { create(:taxon, taxonomy: taxonomy) }
  let(:taxon_2) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon_1], price: 19.90) }
  let!(:related_product) { create(:product, taxons: [taxon_1], price: 19.99) }
  let!(:unrelated_product) { create(:product, taxons: [taxon_2], price: 20.00) }

  describe 'product display' do
    before do
      visit potepan_product_path(product.id)
    end

    it 'displays a product page correctly' do
      expect(page).to have_title "#{product.name} - BIGBAG Store"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).not_to have_content unrelated_product.name
      expect(page).not_to have_content unrelated_product.display_price
    end

    it 'operates HOME link correctly' do
      click_link 'HOME', match: :prefer_exact
      expect(current_path).to eq potepan_path
    end

    it 'operates 一覧ページへ戻る link correctly' do
      click_link '一覧ページへ戻る', match: :prefer_exact
      expect(current_path).to eq potepan_category_path(taxon_1.id)
    end

    it 'operates a related product link correctly' do
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end
end
