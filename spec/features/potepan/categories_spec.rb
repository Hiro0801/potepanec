require 'rails_helper'

RSpec.feature "categories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon_1) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:taxon_2) { create(:taxon, taxonomy: taxonomy) }
  let!(:taxon_1_product) { create(:product, taxons: [taxon_1], price: 19.99) }
  let!(:taxon_2_product) { create(:product, taxons: [taxon_2], price: 20.00) }

  describe 'category display' do
    before do
      visit potepan_category_path(taxon_1.id)
    end

    it 'displays a category page correctly' do
      expect(page).to have_title "#{taxon_1.name} - BIGBAG Store"
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon_1.name
      expect(page).to have_content taxon_1.products.count
      expect(page).to have_content taxon_2.name
      expect(page).to have_content taxon_2.products.count
      expect(page).to have_content taxon_1_product.name
      expect(page).to have_content taxon_1_product.display_price
      expect(page).not_to have_content taxon_2_product.name
      expect(page).not_to have_content taxon_2_product.display_price
    end

    it 'operates a category link correctly' do
      click_link "#{taxon_1.name} (#{taxon_1.products.count})"
      expect(current_path).to eq potepan_category_path(taxon_1.id)
    end

    it 'operates a product link correctly' do
      click_link taxon_1_product.name
      expect(current_path).to eq potepan_product_path(taxon_1_product.id)
    end
  end
end
