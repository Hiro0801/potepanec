require 'rails_helper'

RSpec.feature "HomePages", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe 'home_pages display' do
    before do
      visit potepan_path
    end

    it 'displays a category page correctly' do
      expect(page).to have_title "BIGBAG Store"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end

    it 'operates a product link correctly' do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
