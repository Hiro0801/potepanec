require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  let(:product) { create(:product) }
  let(:taxon) { create(:taxon) }

  describe 'full title' do
    it 'displays a top page title correctly' do
      expect(full_title('')).to eq 'BIGBAG Store'
    end

    it 'displays a product page title correctly' do
      expect(full_title(product.name)).to eq "#{product.name} - BIGBAG Store"
    end

    it 'displays a category page title correctly' do
      expect(full_title(taxon.name)).to eq "#{taxon.name} - BIGBAG Store"
    end
  end
end
