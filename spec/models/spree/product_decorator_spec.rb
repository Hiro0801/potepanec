require 'rails_helper'

RSpec.describe Spree::ProductDecorator, type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon_1) { create(:taxon, taxonomy: taxonomy) }
  let(:taxon_2) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon_1]) }
  let(:related_product) { create(:product, taxons: [taxon_1]) }
  let(:unrelated_product) { create(:product, taxons: [taxon_2]) }
  let!(:old_product) { create(:product, available_on: 2.day.ago) }
  let!(:new_product) { create(:product, available_on: 1.day.ago) }

  describe 'related_products' do
    it 'has a related product only' do
      expect(product.related_products).to eq [related_product]
    end

    it 'does not have a product itself' do
      expect(product.related_products).not_to include product
    end

    it 'does not have an unrelated product' do
      expect(product.related_products).not_to include unrelated_product
    end
  end

  describe 'latest_order' do
    it 'returns new_product first' do
      expect(Spree::Product.first).to eq old_product
      expect(Spree::Product.latest_order.first).to eq new_product
    end
  end
end
