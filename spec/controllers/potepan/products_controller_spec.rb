require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it 'responds successsfully' do
      expect(response).to be_success
    end

    it 'returns a 200 response' do
      expect(response).to have_http_status "200"
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end

    it 'assigns the requested product to @product' do
      expect(assigns(:product)).to eq product
    end

    it 'assigns the requested related_products to @related_products' do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
