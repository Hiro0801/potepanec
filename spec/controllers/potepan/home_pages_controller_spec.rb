require 'rails_helper'

RSpec.describe Potepan::HomePagesController, type: :controller do
  describe 'GET #index' do
    let!(:new_products) { create_list(:product, 10, available_on: 1.week.ago) }
    let(:old_products) { create_list(:product, 5, available_on: 1.year.ago) }
    let!(:latest) { create(:product, available_on: 1.day.ago) }
    let(:future_product) { create(:product, available_on: 1.day.from_now) }

    before do
      get :index
    end

    it 'responds successsfully' do
      expect(response).to be_success
    end

    it 'returns a 200 response' do
      expect(response).to have_http_status "200"
    end

    it 'renders the :index template' do
      expect(response).to render_template :index
    end

    describe 'assigns the requested new_products to @new_products' do
      it 'has 8 products' do
        expect(assigns(:new_products).count).to eq 8
      end

      it "returns latest first" do
        expect(assigns(:new_products).first).to eq latest
      end

      it "does not have old_products" do
        expect(assigns(:new_products)).not_to include(old_products)
      end

      it "does not have future_product" do
        expect(assigns(:new_products)).not_to include(future_product)
      end
    end
  end
end
