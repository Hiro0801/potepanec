require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it 'responds successsfully' do
      expect(response).to be_success
    end

    it 'returns a 200 response' do
      expect(response).to have_http_status "200"
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end

    it 'assigns the requested taxonomies to @taxonomies' do
      expect(assigns(:taxonomies)).to eq [taxonomy]
    end

    it 'assigns the requested taxon to @taxon' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'assigns the requested products to @products' do
      expect(assigns(:products)).to eq [product]
    end
  end
end
